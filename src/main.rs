extern crate env_logger;
#[macro_use]
extern crate log;
#[macro_use]
extern crate maidsafe_utilities;
extern crate time;

static INITIALISE_LOGGER: ::std::sync::Once = ::std::sync::ONCE_INIT;

pub fn initialise_logger(show_thread_name: bool) {
    INITIALISE_LOGGER.call_once(|| {
        let format = move |record: &::log::LogRecord| {
            let now = ::time::now();
            let mut thread_name = "".to_string();
            if show_thread_name {
                thread_name = ::std::thread::current().name().unwrap_or("???").to_owned() + " ";
            }
            format!("{} {}.{:06} {}[{}:{}:{}] {}",
                match record.level() {
                    ::log::LogLevel::Error => 'E',
                    ::log::LogLevel::Warn => 'W',
                    ::log::LogLevel::Info => 'I',
                    ::log::LogLevel::Debug => 'D',
                    ::log::LogLevel::Trace => 'T',
                },
                ::time::strftime("%T", &now).unwrap(),
                now.tm_nsec / 1000,
                thread_name,
                record.location().module_path().splitn(2, "::").next().unwrap_or(""),
                record.location().file(),
                record.location().line(),
                record.args())
        };

        let mut builder = ::env_logger::LogBuilder::new();
        let _ = builder.format(format);

        if let Ok(rust_log) = ::std::env::var("RUST_LOG") {
            let _ = builder.parse(&rust_log);
        }

        builder.init().unwrap_or_else(|error| println!("Error initialising logger: {}", error));
    });
}

fn main() {
    initialise_logger(true);

    error!("Error level message.");
    warn!("Warning level message.");
    info!("Info level message.");
    debug!("Debug level message.");
    trace!("Trace level message.");

    let unnamed = ::std::thread::spawn(move || info!("Message in unnamed thread."));
    let _ = unnamed.join();

    let _named = ::maidsafe_utilities::thread::RaiiThreadJoiner::new(thread!("Worker",
        move || warn!("Message in named thread.")));
}
