# Env Logger Example

This is an example of using an [`env_logger::LogBuilder`](http://rust-lang-nursery.github.io/log/env_logger/struct.LogBuilder.html) to create custom log output.
